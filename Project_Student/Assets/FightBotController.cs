﻿using UnityEngine;
using System.Collections;

public class FightBotController : MonoBehaviour {

	public Animator FightBotAnimator;

	void Start() {

	}

	// Update is called once per frame
	void Update() {
		if (Input.GetKey (KeyCode.W)) {
			FightBotAnimator.SetFloat ("Speed", 1);
		}
		else {
			FightBotAnimator.SetFloat ("Speed", 0);
		}

		if (Input.GetKey (KeyCode.LeftShift)) {
			FightBotAnimator.SetBool ("Sprint", true);
		} else {
			FightBotAnimator.SetBool ("Sprint", false);
		}

		if (Input.GetKeyDown (KeyCode.Space)) {
			FightBotAnimator.SetTrigger ("Jump");
		}


		if (Input.GetKeyDown (KeyCode.E)) {
			FightBotAnimator.SetTrigger ("Rolling");
		}

		//if (Input.GetKeyDown (KeyCode.D)) {
		//	SkeletonAnimator.SetBool ("Attack" , true);
		//}

		//if (Input.GetKeyUp (KeyCode.D)) {
		//	SkeletonAnimator.SetBool ("Attack" , false	);
		//}


	}
}
